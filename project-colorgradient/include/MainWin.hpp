#ifndef MAINWIN_H
#define MAINWIN_H

#include "AbstractMainWin.hpp"
#include "gradient/GradientSelector.hpp"

class MainWin : public AbstractMainWin
{
	Q_OBJECT
  public:
	MainWin()
	    : dialog(gradient){};

  protected:
	// declare drawn resources
	virtual void initScene() override;

	// update physics/controls/meshes, etc...
	// prepare for rendering
	virtual void updateScene(BasicCamera& camera,
	                         QString const& pathId) override;

	// render user scene on camera
	// (no controllers or hands)
	virtual void renderScene(BasicCamera const& camera,
	                         QString const& pathId) override;

	virtual void applyPostProcShaderParams(
	    QString const& id, GLShaderProgram const& shader,
	    GLFramebufferObject const& currentTarget) const override;

  private:
	Gradient gradient;

	GradientSelector dialog;
};

#endif // MAINWIN_H
