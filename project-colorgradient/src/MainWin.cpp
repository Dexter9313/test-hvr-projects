#include "MainWin.hpp"

void MainWin::initScene()
{
	renderer.appendPostProcessingShader("preview", "preview");

	dialog.show();

	connect(&dialog, &GradientSelector::gradientChanged,
	        [this]()
	        {
		        qDebug() << gradient.start.getAs(Color::Space::CIELAB);
		        qDebug() << gradient.c1;
		        qDebug() << gradient.c2;
		        qDebug() << gradient.end.getAs(Color::Space::CIELAB);
	        });
}

void MainWin::updateScene(BasicCamera& /*camera*/, QString const& /*pathId*/) {}

void MainWin::renderScene(BasicCamera const& /*camera*/,
                          QString const& /*pathId*/)
{
}

void MainWin::applyPostProcShaderParams(
    QString const& id, GLShaderProgram const& shader,
    GLFramebufferObject const& currentTarget) const
{
	if(id == "preview")
	{
		gradient.setShaderUniforms(shader);
	}
	else
	{
		AbstractMainWin::applyPostProcShaderParams(id, shader, currentTarget);
	}
}
