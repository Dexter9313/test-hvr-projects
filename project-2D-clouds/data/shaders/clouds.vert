#version 150 core

in vec2 position;

uniform vec2 center = vec2(0.5, 0.5);
uniform float scale = 1.0;

out vec2 texCoord;

void main()
{
	texCoord = position;
	texCoord *= scale;
	texCoord += center;
	texCoord.y = 1.0 - texCoord.y;
	gl_Position = vec4(position*2.0, 0.0, 1.0);
}
