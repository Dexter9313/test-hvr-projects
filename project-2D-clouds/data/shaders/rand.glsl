
float rand(vec2 c)
{
	return fract(sin(dot(c.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

// random white noise
vec4 rand4(vec2 v)
{
	return vec4(rand(v), rand(v + vec2(1.0)), rand(v + vec2(1.2)),
	            rand(v + vec2(3.0)));
}

float smooth_(float x)
{
	float xsq = x * x;
	return 3.0 * xsq - 2.0 * x * xsq;
}

// for a given in-integer grid point, evaluates a polynomial with coefficients
// tied to the corners
float polynoise(vec2 v)
{
	vec4 coeffs = vec4(
	    rand(floor(v) / 1000.0), rand(vec2(ceil(v.x), floor(v.y)) / 1000.0),
	    rand(vec2(floor(v.x), ceil(v.y)) / 1000.0), rand(ceil(v) / 1000.0));

	vec2 vf = fract(v);

	return coeffs.x + (coeffs.y - coeffs.x) * smooth_(vf.x)
	       + (coeffs.z - coeffs.x) * smooth_(vf.y)
	       + (coeffs.x - coeffs.y - coeffs.z + coeffs.w) * smooth_(vf.x)
	             * smooth_(vf.y);
}

float fbm(vec2 v, vec2 size, int octaves)
{
	float result = 0.0;
	float fac = 0.5;
	const mat2 base_rot = mat2(4.0/5.0, 3.0/5.0, -3.0/5.0, 4.0/5.0);
	mat2 rot = mat2(1.0, 0.0, 0.0, 1.0);
	for(int i = 0; i < octaves; ++i)
	{
		result += fac * polynoise(rot * v * size);
		size *= 2.0;
		fac *= 0.5;
		rot *= base_rot;
	}

	return result;
}
