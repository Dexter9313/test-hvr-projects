#version 150

in vec2 texCoord;

uniform vec2 texsize;
uniform float useNoise = 1.0;

out vec4 outColor;

uniform sampler2D debuggedTex;

#include <rand.glsl>

float lin(float x0, float x1, float t)
{
	return t*x1 + (1.0-t)*x0;
}

void main()
{
	vec4 c = texture(debuggedTex, texCoord);
	outColor = c;
	if(useNoise == 1.0)
	{
		outColor *= vec4(lin(0.6, 1.4, fbm(texCoord, texsize, 10)));
	}

	// outColor.rgb = vec3(fbm(texCoord, vec2(1.0, 1.0), 10));
}
