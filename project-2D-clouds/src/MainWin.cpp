#include "MainWin.hpp"

#include "utils.hpp"

void MainWin::keyPressEvent(QKeyEvent* e)
{
	if(e->key() == Qt::Key_PageUp)
	{
		scale *= 0.9f;
		qDebug() << scale;
		return;
	}
	if(e->key() == Qt::Key_PageDown)
	{
		scale /= 0.9f;
		return;
	}
	if(e->key() == Qt::Key_Left)
	{
		center[0] -= 0.03f * scale;
		return;
	}
	if(e->key() == Qt::Key_Right)
	{
		center[0] += 0.03f * scale;
		return;
	}
	if(e->key() == Qt::Key_Down)
	{
		center[1] -= 0.03f * scale;
		return;
	}
	if(e->key() == Qt::Key_Up)
	{
		center[1] += 0.03f * scale;
		return;
	}
	if(e->key() == Qt::Key_Space)
	{
		useNoise = !useNoise;
		return;
	}

	AbstractMainWin::keyPressEvent(e);
}

void MainWin::initScene()
{
	cloudTex = new GLTexture(
	    getAbsoluteDataPath("images/clouds.jpg").toLatin1().data());
	quad   = new GLMesh;
	shader = new GLShaderProgram("clouds");

	Primitives::setAsQuad(*quad, *shader);
	shader->setUniform("texsize", QVector2D(cloudTex->getSize().width(),
	                                        cloudTex->getSize().height()));
}

void MainWin::updateScene(BasicCamera& /*camera*/, QString const& /*pathId*/)
{
	shader->setUniform("center", center);
	shader->setUniform("scale", scale);
	shader->setUniform("useNoise", useNoise ? 1.f : 0.f);
}

void MainWin::renderScene(BasicCamera const& /*camera*/,
                          QString const& /*pathId*/)
{
	GLHandler::setBackfaceCulling(false);
	GLHandler::useTextures({cloudTex});
	GLHandler::setUpRender(*shader);
	quad->render(PrimitiveType::TRIANGLE_STRIP);
	GLHandler::setBackfaceCulling(true);
}
